/**
 * @file lora_handler.cpp
 * @author Bernd Giesecke (bernd.giesecke@rakwireless.com)
 * @brief Initialization, event handlers and task for LoRaWan
 * @version 0.1
 * @date 2020-08-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include "main.h"

/** DIO1 GPIO pin for RAK4631 */
#define PIN_LORA_DIO_1 47

/** Max size of the data to be transmitted. */
#define LORAWAN_APP_DATA_BUFF_SIZE 64
/** Number of trials for the join request. */
#define JOINREQ_NBTRIALS 8

/** Lora application data buffer. */
static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];
/** Lora application data structure. */
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0};

// LoRaWan event handlers
/** LoRaWan callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWan callback when join failed */
static void lorawan_join_failed_handler(void);
/** LoRaWan callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWan callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWan Function to send a package */
bool sendLoRaFrame(void);

uint32_t vbat_pin = PIN_VBAT;
extern Adafruit_BME680 bme;
SFE_UBLOX_GNSS g_myGNSS;
long g_lastTime = 0; //Simple local timer. Limits amount if I2C traffic to u-blox module.
bool GPSativo = false;

void data_get(void);
String comando_at(){
  String resp = "";

  Serial1.readString();
  Serial1.write("AT+CWLAPOPT=1,12\r\n");
  delay(100);
  Serial1.readString();
  
  Serial1.write("AT+CWLAP=\r\n");
  delay(1);
  int32_t timeout = 5000;
  while (timeout--) {
    if (Serial1.available())  {
      resp += Serial1.readString();
    }
    delay(1);
  }
  resp.toUpperCase();
  Serial1.write("AT+GSLP=105000\r\n");
  return resp;
}

uint8_t hex8(String in) {
   uint8_t c, h;
   c = in.charAt(0);

   if (c <= '9' && c >= '0') {  c -= '0'; }
   else if (c <= 'f' && c >= 'a') { c -= ('a' - 0x0a); }
   else if (c <= 'F' && c >= 'A') { c -= ('A' - 0x0a); }
   else return(-1);

   h = c;
   c = in.charAt(1);

   if (c <= '9' && c >= '0') {  c -= '0'; }
   else if (c <= 'f' && c >= 'a') { c -= ('a' - 0x0a); }
   else if (c <= 'F' && c >= 'A') { c -= ('A' - 0x0a); }
   else return(-1);

   return ( h<<4 | c);
}

/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 * 
 * Set structure members to
 * LORAWAN_ADR_ON or LORAWAN_ADR_OFF to enable or disable adaptive data rate
 * LORAWAN_DEFAULT_DATARATE OR DR_0 ... DR_5 for default data rate or specific data rate selection
 * LORAWAN_PUBLIC_NETWORK or LORAWAN_PRIVATE_NETWORK to select the use of a public or private network
 * JOINREQ_NBTRIALS or a specific number to set the number of trials to join the network
 * LORAWAN_DEFAULT_TX_POWER or a specific number to set the TX power used
 * LORAWAN_DUTYCYCLE_ON or LORAWAN_DUTYCYCLE_OFF to enable or disable duty cycles
 *                   Please note that ETSI mandates duty cycled transmissions. 
 */
static lmh_param_t lora_param_init = {LORAWAN_ADR_ON, DR_3, LORAWAN_PUBLIC_NETWORK, JOINREQ_NBTRIALS, LORAWAN_DEFAULT_TX_POWER, LORAWAN_DUTYCYCLE_OFF};

uint8_t get_lora_batt(void);
/** Structure containing LoRaWan callback functions, needed for lmh_init() */
static lmh_callback_t lora_callbacks = {get_lora_batt, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_failed_handler};

////OTAA keys !!!! KEYS ARE MSB !!!!
uint8_t nodeDeviceEUI[8] = {0xAC, 0x1F, 0x09, 0xFF, 0xFE, 0x05, 0x06, 0x29};
uint8_t nodeAppEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t nodeAppKey[16] = { 0x12, 0x0f, 0x5b, 0x8b, 0x23, 0x64, 0x51, 0x7f, 0x87, 0xe9, 0xd6, 0x90, 0x0e, 0x24, 0x2d, 0x04};

/** Device address required for ABP network join */
uint32_t nodeDevAddr = 0x26021FB6;
/** Network session key required for ABP network join */
uint8_t nodeNwsKey[16] = {0x32, 0x3D, 0x15, 0x5A, 0x00, 0x0D, 0xF3, 0x35, 0x30, 0x7A, 0x16, 0xDA, 0x0C, 0x9D, 0xF5, 0x3F};
/** Application session key required for ABP network join */
uint8_t nodeAppsKey[16] = {0x3F, 0x6A, 0x66, 0x45, 0x9D, 0x5E, 0xDC, 0xA6, 0x3C, 0xBC, 0x46, 0x19, 0xCD, 0x61, 0xA1, 0x1E};

/** Flag whether to use OTAA or ABP network join method */
bool doOTAA = true;

DeviceClass_t gCurrentClass = CLASS_A;           /* class definition*/
LoRaMacRegion_t gCurrentRegion = LORAMAC_REGION_AU915; /* Region:EU868*/

/**
 * @brief Initialize LoRa HW and LoRaWan MAC layer
 * 
 * @return int8_t result
 *  0 => OK
 * -1 => SX126x HW init failure
 * -2 => LoRaWan MAC initialization failure
 * -3 => Subband selection failure
 * -4 => LoRaWan handler task start failure
 */
int8_t initLoRaWan(void)
{
  Serial.printf("<<>> lora_rak4630_init called at %dms\n", millis());
  // Initialize LoRa chip.
  if (lora_rak4630_init() != 0)
  {
    return -1;
  }
  Serial.printf("<<>> lora_rak4630_init finished at %dms\n", millis());
  delay(100);

  // Setup the EUIs and Keys
  if (doOTAA)
  {
    lmh_setDevEui(nodeDeviceEUI);
    lmh_setAppEui(nodeAppEUI);
    lmh_setAppKey(nodeAppKey);
  }
  else
  {
    lmh_setNwkSKey(nodeNwsKey);
    lmh_setAppSKey(nodeAppsKey);
    lmh_setDevAddr(nodeDevAddr);
  }

  Serial.printf("<<>> lmh_init called at %dms\n", millis());
  // Initialize LoRaWan
  if (lmh_init(&lora_callbacks, lora_param_init, doOTAA, gCurrentClass, gCurrentRegion) != 0)
  {
    return -2;
  }
  Serial.printf("<<>> lmh_init finished at %dms\n", millis());
  delay(100);

  // For some regions we might need to define the sub band the gateway is listening to
  // This must be called AFTER lmh_init()
  if (!lmh_setSubBandChannels(2))
  {
    return -3;
  }

  // Start Join procedure
#ifndef MAX_SAVE
  Serial.printf("<<>> lmh_join called at %dms\n", millis());
#endif
  lmh_join();
  Serial.printf("<<>> lmh_join finished at %dms\n", millis());
  delay(100);

  return 0;
}

/**
 * @brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{
  Serial.printf("<<>> lorawan_has_joined_handler called at %dms\n", millis());
  delay(100);
  if (doOTAA)
  {
    uint32_t otaaDevAddr = lmh_getDevAddr();
#ifndef MAX_SAVE
    Serial.printf("OTAA joined and got dev address %08X\n", otaaDevAddr);
#endif
  }
  else
  {
#ifndef MAX_SAVE
    Serial.println("ABP joined");
#endif
  }

  // Default is Class A, where the SX1262 transceiver is in sleep mode unless a package is sent
  // If switched to Class C the power consumption is higher because the SX1262 chip remains in RX mode

  // lmh_class_request(CLASS_C);

  digitalWrite(LED_CONN, LOW);

  // Now we are connected, start the timer that will wakeup the loop frequently
  taskWakeupTimer.begin(SLEEP_TIME, periodicWakeup);
  taskWakeupTimer.start();

  eventType = 1;
  // Notify task about the event
  if (taskEvent != NULL)
  {
    Serial.printf("<<>> Waking up task at %dms\n", millis());
    xSemaphoreGive(taskEvent);
  }
}

/**@brief LoRa function for handling OTAA join failed
*/
static void lorawan_join_failed_handler(void)
{
  Serial.println("OVER_THE_AIR_ACTIVATION failed!");
  Serial.println("Check your EUI's and Keys's!");
  Serial.println("Check if a Gateway is in range!");
}
/**
 * @brief Function for handling LoRaWan received data from Gateway
 *
 * @param app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  if(app_data->buffer[0] == '1')  {
    GPSativo = true;  
  } else if(app_data->buffer[0] == '0') {
    GPSativo = false;  
  }
#ifndef MAX_SAVE
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);
#endif
  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
#ifndef MAX_SAVE
        Serial.println("Request to switch to class A");
#endif
        break;

      case 1:
        lmh_class_request(CLASS_B);
#ifndef MAX_SAVE
        Serial.println("Request to switch to class B");
#endif
        break;

      case 2:
        lmh_class_request(CLASS_C);
#ifndef MAX_SAVE
        Serial.println("Request to switch to class C");
#endif
        break;

      default:
        break;
      }
    }

    break;
  case LORAWAN_APP_PORT:
    // Copy the data into loop data buffer
    memcpy(rcvdLoRaData, app_data->buffer, app_data->buffsize);
    rcvdDataLen = app_data->buffsize;
    eventType = 0;
    // Notify task about the event
    if (taskEvent != NULL)
    {
#ifndef MAX_SAVE
      Serial.println("Waking up loop task");
#endif
      xSemaphoreGive(taskEvent);
    }

  }
}

/**
 * @brief Callback for class switch confirmation
 * 
 * @param Class The new class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
#ifndef MAX_SAVE
  Serial.printf("switch to class %c done\n", "ABC"[Class]);
#endif

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, LMH_UNCONFIRMED_MSG);
}

/**
 * @brief Send a LoRaWan package
 * 
 * @return result of send request
 */
bool sendLoRaFrame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
#ifndef MAX_SAVE
    Serial.println("Did not join network, skip sending frame");
#endif
    return false;
  }

  //m_lora_app_data.port = LORAWAN_APP_PORT;

  //******************************************************************
  /// \todo here some more usefull data should be put into the package
  //******************************************************************

  data_get();
  
  //m_lora_app_data.buffsize = buffSize;

  lmh_error_status error = lmh_send(&m_lora_app_data, LMH_UNCONFIRMED_MSG);

  return (error == 0);
}

void bme680_get() {
  if (!bme.performReading()) {
    return;
  }
  double temp = bme.temperature;
  double pres = bme.pressure / 100.0;
  double hum = bme.humidity;
  uint32_t gas = bme.gas_resistance;
}

void data_get() {
  #ifdef DEBUG_ATIVO
  Serial.print("result: ");
  #endif
  uint32_t i = 0;
  memset(m_lora_app_data.buffer, 0, LORAWAN_APP_DATA_BUFF_SIZE);

  //inicia comunicação
  Serial1.begin(115200);
  delay(1000);
  Serial1.setTimeout(1500);
  Serial1.readString();
  
  bme680_get();
  double temp = bme.temperature;
  double hum = bme.humidity;
  uint16_t t = temp * 100;
  uint16_t h = hum * 100;

  uint8_t vbat_per = get_lora_batt()/2.54;

  String resp = comando_at();

  if(!GPSativo) {
    m_lora_app_data.port=1;
    m_lora_app_data.buffer[i++] = 0x01;
    
    m_lora_app_data.buffer[i++] = hex8(resp.substring(24,26));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(27,29));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(30,32));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(33,35));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(36,38));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(39,41));
    m_lora_app_data.buffer[i++] = (uint8_t)(((resp.charAt(20) & 0xf) * 10) + ((resp.charAt(21) & 0xf) * 1));
    
    m_lora_app_data.buffer[i++] = hex8(resp.substring(58,60));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(61,63));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(64,66));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(67,69));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(70,72));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(73,75));
    m_lora_app_data.buffer[i++] = (uint8_t)(((resp.charAt(54) & 0xf) * 10) + ((resp.charAt(55) & 0xf) * 1));
    
    m_lora_app_data.buffer[i++] = hex8(resp.substring(92,94));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(95,97));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(98,100));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(101,103));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(104,106));
    m_lora_app_data.buffer[i++] = hex8(resp.substring(107,109));
    m_lora_app_data.buffer[i++] = (uint8_t)(((resp.charAt(88) & 0xf) * 10) + ((resp.charAt(89) & 0xf) * 1));
  } else  { 
    digitalWrite(WB_IO2, 1);
    delay(2000);

    int32_t latitude = 0;
    int32_t longitude = 0;
    int32_t altitude = 0;
    if (g_myGNSS.begin() == true) {  //Connect to the u-blox module using Wire port
      g_myGNSS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
      g_myGNSS.saveConfigSelective(VAL_CFG_SUBSEC_IOPORT); //Save (only) the communications port settings to flash and BBR
      delay(10000);
      latitude = g_myGNSS.getLatitude();
      longitude = g_myGNSS.getLongitude();
      altitude = g_myGNSS.getAltitude();
    }
  
    int cont = 0;
    while(latitude == 0 && longitude == 0 && altitude == 0){
      delay(5000);
      latitude = g_myGNSS.getLatitude();
      longitude = g_myGNSS.getLongitude();
      altitude = g_myGNSS.getAltitude();
      cont++;
      if(cont >= 5) break;
    }

    g_myGNSS.end();

    if(latitude != 0 && longitude != 0) {
      m_lora_app_data.port  = 2;
      m_lora_app_data.buffer[i++] = 0x02; //GPS
      
      m_lora_app_data.buffer[i++] = (uint8_t)(latitude >> 24);
      m_lora_app_data.buffer[i++] = (uint8_t)(latitude >> 16);
      m_lora_app_data.buffer[i++] = (uint8_t)(latitude >> 8);
      m_lora_app_data.buffer[i++] = (uint8_t)latitude;
      
      m_lora_app_data.buffer[i++] = (uint8_t)(longitude >> 24);
      m_lora_app_data.buffer[i++] = (uint8_t)(longitude >> 16);
      m_lora_app_data.buffer[i++] = (uint8_t)(longitude >> 8);
      m_lora_app_data.buffer[i++] = (uint8_t)longitude;
  
      m_lora_app_data.buffer[i++] = (uint8_t)(altitude >> 24);
      m_lora_app_data.buffer[i++] = (uint8_t)(altitude >> 16);
      m_lora_app_data.buffer[i++] = (uint8_t)(altitude >> 8);
      m_lora_app_data.buffer[i++] = (uint8_t)altitude;
    } else  {
      m_lora_app_data.port  = 3;
      m_lora_app_data.buffer[i++] = 0x03; //  Nenhuma informação
    }
  }

  //finaliza comunicação
  delay(1000);
  Serial1.end();
  digitalWrite(WB_IO2, 0);
  delay(1);
  m_lora_app_data.buffer[i++] = (uint8_t)(t >> 8);
  m_lora_app_data.buffer[i++] = (uint8_t)t;
  m_lora_app_data.buffer[i++] = (uint8_t)(h >> 8);
  m_lora_app_data.buffer[i++] = (uint8_t)h;

  m_lora_app_data.buffer[i++] = vbat_per;

  m_lora_app_data.buffsize = i;
}

float readVBAT(void)  {
  float raw;
  raw = analogRead(vbat_pin);// Get the raw 12-bit, 0..3000mV ADC value
  return raw * REAL_VBAT_MV_PER_LSB;
}

uint8_t mvToPercent(float mvolts) {
  if (mvolts < 3300)  return 0;
  if (mvolts < 3600)  {
    mvolts -= 3300;
    return mvolts / 30;
  }
  mvolts -= 3600;
  return 10 + (mvolts * 0.15F); // thats mvolts /6.66666666
}

uint8_t get_lora_batt(void) {
  uint16_t read_val = 0;
  for (int i = 0; i < 10; i++)  {
    read_val += readVBAT();
  }
  return (mvToPercent(read_val/10) * 2.54);
}
