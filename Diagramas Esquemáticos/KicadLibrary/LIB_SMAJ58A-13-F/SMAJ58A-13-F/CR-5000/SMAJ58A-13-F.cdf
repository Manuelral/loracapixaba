(part "SMAJ58A-13-F"
    (packageRef "DIOM5226X230N")
    (interface
        (port "1" (symbPinId 1) (portName "K") (portType INOUT))
        (port "2" (symbPinId 2) (portName "A") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "Manufacturer_Name" "Diodes Inc.")
    (property "Manufacturer_Part_Number" "SMAJ58A-13-F")
    (property "Mouser_Part_Number" "621-SMAJ58A-13-F")
    (property "Mouser_Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=621-SMAJ58A-13-F")
    (property "RS_Part_Number" "7515010P")
    (property "RS_Price/Stock" "http://uk.rs-online.com/web/p/products/7515010P")
    (property "Allied_Number" "70438548")
    (property "Allied_Price/Stock" "https://www.alliedelec.com/diodes-inc-smaj58a-13-f/70438548/")
    (property "Description" "Diodes Inc SMAJ58A-13-F, Uni-Directional TVS Diode, 400W, 2-Pin DO-214AC")
    (property "Datasheet_Link" "https://componentsearchengine.com/Datasheets/1/SMAJ58A-13-F.pdf")
    (property "symbolName1" "SMAJ58A-13-F")
)
