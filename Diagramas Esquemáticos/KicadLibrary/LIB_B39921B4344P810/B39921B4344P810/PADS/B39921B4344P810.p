*PADS-LIBRARY-PART-TYPES-V9*

B39921B4344P810 B39921B4344P810 I ANA 9 1 0 0 0
TIMESTAMP 2020.02.12.17.22.20
"Manufacturer_Name" RF360
"Manufacturer_Part_Number" B39921B4344P810
"Mouser Part Number" 871-B39921B4344P810
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=871-B39921B4344P810
"RS Part Number" 
"RS Price/Stock" 
"Description" Signal Conditioning Cu Frame Filter 1411 915MHz 2.8dB QCS5P
"Datasheet Link" https://componentsearchengine.com/Datasheets/1/B39921B4344P810.pdf
"Geometry.Height" 0.45mm
GATE 1 5 0
B39921B4344P810
1 0 U INPUT
2 0 U GND_1
3 0 U GND_2
4 0 U OUTPUT
5 0 U GND_3

*END*
*REMARK* SamacSys ECAD Model
2403531/180600/2.45/5/4/Filter
