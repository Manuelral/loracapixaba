PULSONIX_LIBRARY_ASCII "SamacSys ECAD Model"
//288688/180600/2.47/6/4/Connector

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "c165_h110"
		(holeDiam 1.1)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.65) (shapeHeight 1.65))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.65) (shapeHeight 1.65))
	)
	(padStyleDef "s165_h110"
		(holeDiam 1.1)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.65) (shapeHeight 1.65))
		(padShape (layerNumRef 16) (padShapeType Rect)  (shapeWidth 1.65) (shapeHeight 1.65))
	)
	(textStyleDef "Normal"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 1.27)
			(strokeWidth 0.127)
		)
	)
	(patternDef "HDRV6W64P254_2X3_1524X508X1104" (originalName "HDRV6W64P254_2X3_1524X508X1104")
		(multiLayer
			(pad (padNum 1) (padStyleRef s165_h110) (pt 0, 0) (rotation 90))
			(pad (padNum 2) (padStyleRef c165_h110) (pt 0, 2.54) (rotation 90))
			(pad (padNum 3) (padStyleRef c165_h110) (pt 2.54, 0) (rotation 90))
			(pad (padNum 4) (padStyleRef c165_h110) (pt 2.54, 2.54) (rotation 90))
			(pad (padNum 5) (padStyleRef c165_h110) (pt 5.08, 0) (rotation 90))
			(pad (padNum 6) (padStyleRef c165_h110) (pt 5.08, 2.54) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0, 0) (textStyleRef "Normal") (isVisible True))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -5.505 -1.52) (pt -5.505 4.06) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -5.505 4.06) (pt 10.585 4.06) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 10.585 4.06) (pt 10.585 -1.52) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 10.585 -1.52) (pt -5.505 -1.52) (width 0.05))
		)
		(layerContents (layerNumRef 28)
			(line (pt -5.255 -1.27) (pt -5.255 3.81) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -5.255 3.81) (pt 10.335 3.81) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 10.335 3.81) (pt 10.335 -1.27) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 10.335 -1.27) (pt -5.255 -1.27) (width 0.025))
		)
		(layerContents (layerNumRef 18)
			(line (pt -5.255 0) (pt -5.255 -1.27) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -5.255 -1.27) (pt 10.335 -1.27) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 10.335 -1.27) (pt 10.335 3.81) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 10.335 3.81) (pt 0 3.81) (width 0.2))
		)
	)
	(symbolDef "M20-9970346" (originalName "M20-9970346")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 3) (pt 0 mils -200 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -225 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 4) (pt 800 mils 0 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 570 mils -25 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 5) (pt 800 mils -100 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 570 mils -125 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 6) (pt 800 mils -200 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 570 mils -225 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(line (pt 200 mils 100 mils) (pt 600 mils 100 mils) (width 6 mils))
		(line (pt 600 mils 100 mils) (pt 600 mils -300 mils) (width 6 mils))
		(line (pt 600 mils -300 mils) (pt 200 mils -300 mils) (width 6 mils))
		(line (pt 200 mils -300 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))
		(attr "Type" "Type" (pt 650 mils 200 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))

	)
	(compDef "M20-9970346" (originalName "M20-9970346") (compHeader (numPins 6) (numParts 1) (refDesPrefix J)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "3" (pinName "3") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "5" (pinName "5") (partNum 1) (symPinNum 3) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 4) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "4" (pinName "4") (partNum 1) (symPinNum 5) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "6" (pinName "6") (partNum 1) (symPinNum 6) (gateEq 0) (pinEq 0) (pinType Unknown))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "M20-9970346"))
		(attachedPattern (patternNum 1) (patternName "HDRV6W64P254_2X3_1524X508X1104")
			(numPads 6)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
				(padNum 3) (compPinRef "3")
				(padNum 4) (compPinRef "4")
				(padNum 5) (compPinRef "5")
				(padNum 6) (compPinRef "6")
			)
		)
		(attr "Manufacturer_Name" "Harwin")
		(attr "Manufacturer_Part_Number" "M20-9970346")
		(attr "Arrow Part Number" "M20-9970346")
		(attr "Arrow Price/Stock" "")
		(attr "Mouser Part Number" "855-M20-9970346")
		(attr "Mouser Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=855-M20-9970346")
		(attr "Description" "HARWIN Pin Header M20, 6 Way, 2.54mm Pitch, 2 Row, Straight, Through Hole")
		(attr "<Hyperlink>" "https://cdn.harwin.com/pdfs/M20-997.pdf")
		(attr "<Component Height>" "11.04")
		(attr "<STEP Filename>" "M20-9970346.stp")
		(attr "<STEP Offsets>" "X=0;Y=0;Z=0")
		(attr "<STEP Rotation>" "X=0;Y=0;Z=0")
	)

)
