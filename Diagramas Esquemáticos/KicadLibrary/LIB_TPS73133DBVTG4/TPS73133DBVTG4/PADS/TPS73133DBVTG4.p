*PADS-LIBRARY-PART-TYPES-V9*

TPS73133DBVTG4 SOT95P280X145-5N I ANA 9 1 0 0 0
TIMESTAMP 2020.02.07.12.35.17
"Manufacturer_Name" Texas Instruments
"Manufacturer_Part_Number" TPS73133DBVTG4
"Mouser Part Number" 595-TPS73133DBVTG4
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPS73133DBVTG4
"RS Part Number" 
"RS Price/Stock" 
"Description" TEXAS INSTRUMENTS - TPS73133DBVTG4 - V REG LDO 0.15A +3.3V, SMD, 73133
"Datasheet Link" http://www.ti.com/general/docs/lit/getliterature.tsp?genericPartNumber=TPS731&&fileType=pdf
"Geometry.Height" 1.45mm
GATE 1 5 0
TPS73133DBVTG4
1 0 U IN
2 0 U GND
3 0 U EN
4 0 U NR/FB
5 0 U OUT

*END*
*REMARK* SamacSys ECAD Model
847118/180600/2.45/5/3/Integrated Circuit
