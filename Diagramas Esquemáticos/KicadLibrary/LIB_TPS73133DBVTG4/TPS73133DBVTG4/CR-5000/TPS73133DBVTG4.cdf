(part "TPS73133DBVTG4"
    (packageRef "SOT95P280X145-5N")
    (interface
        (port "1" (symbPinId 1) (portName "IN") (portType INOUT))
        (port "2" (symbPinId 2) (portName "GND") (portType INOUT))
        (port "3" (symbPinId 3) (portName "EN") (portType INOUT))
        (port "4" (symbPinId 4) (portName "NR/FB") (portType INOUT))
        (port "5" (symbPinId 5) (portName "OUT") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "101")
    (property "Manufacturer_Name" "Texas Instruments")
    (property "Manufacturer_Part_Number" "TPS73133DBVTG4")
    (property "Mouser_Part_Number" "595-TPS73133DBVTG4")
    (property "Mouser_Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPS73133DBVTG4")
    (property "RS_Part_Number" "")
    (property "RS_Price/Stock" "")
    (property "Description" "TEXAS INSTRUMENTS - TPS73133DBVTG4 - V REG LDO 0.15A +3.3V, SMD, 73133")
    (property "Datasheet_Link" "http://www.ti.com/general/docs/lit/getliterature.tsp?genericPartNumber=TPS731&&fileType=pdf")
    (property "symbolName1" "TPS73133DBVTG4")
)
