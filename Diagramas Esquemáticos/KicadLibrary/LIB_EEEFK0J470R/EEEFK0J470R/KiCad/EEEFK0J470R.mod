PCBNEW-LibModule-V1  2020-06-13 20:29:26
# encoding utf-8
Units mm
$INDEX
CAPAE530X610N
$EndINDEX
$MODULE CAPAE530X610N
Po 0 0 0 15 5ee52916 00000000 ~~
Li CAPAE530X610N
Cd size code c
Kw Capacitor Polarised
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "C**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "CAPAE530X610N"
DS -4.55 -3.25 4.55 -3.25 0.05 24
DS 4.55 -3.25 4.55 3.25 0.05 24
DS 4.55 3.25 -4.55 3.25 0.05 24
DS -4.55 3.25 -4.55 -3.25 0.05 24
DS 2.65 -2.65 -1.325 -2.65 0.1 24
DS -1.325 -2.65 -2.65 -1.325 0.1 24
DS -2.65 -1.325 -2.65 1.325 0.1 24
DS -2.65 1.325 -1.325 2.65 0.1 24
DS -1.325 2.65 2.65 2.65 0.1 24
DS 2.65 2.65 2.65 -2.65 0.1 24
DS -4.05 -2.65 2.65 -2.65 0.2 21
DS -1.325 2.65 2.65 2.65 0.2 21
$PAD
Po -2.55 0
Sh "1" R 1.8 3 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.55 0
Sh "2" R 1.8 3 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE CAPAE530X610N
$EndLIBRARY
