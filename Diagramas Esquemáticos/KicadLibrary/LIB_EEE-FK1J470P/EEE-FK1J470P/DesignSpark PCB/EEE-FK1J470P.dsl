SamacSys ECAD Model
282654/180600/2.45/2/3/Capacitor Polarised

DESIGNSPARK_INTERMEDIATE_ASCII

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "r420_210"
		(holeDiam 0)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 2.1) (shapeHeight 4.2))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 0) (shapeHeight 0))
	)
	(textStyleDef "Default"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 50 mils)
			(strokeWidth 5 mils)
		)
	)
	(patternDef "CAPAE830X1050N" (originalName "CAPAE830X1050N")
		(multiLayer
			(pad (padNum 1) (padStyleRef r420_210) (pt -3.7, 0) (rotation 90))
			(pad (padNum 2) (padStyleRef r420_210) (pt 3.7, 0) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0, 0) (textStyleRef "Default") (isVisible True))
		)
		(layerContents (layerNumRef 30)
			(line (pt -6.3 4.75) (pt 6.3 4.75) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt 6.3 4.75) (pt 6.3 -4.75) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt 6.3 -4.75) (pt -6.3 -4.75) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt -6.3 -4.75) (pt -6.3 4.75) (width 0.05))
		)
		(layerContents (layerNumRef 28)
			(line (pt 4.15 4.15) (pt -2.075 4.15) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.075 4.15) (pt -4.15 2.075) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -4.15 2.075) (pt -4.15 -2.075) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -4.15 -2.075) (pt -2.075 -4.15) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.075 -4.15) (pt 4.15 -4.15) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt 4.15 -4.15) (pt 4.15 4.15) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -5.8 4.15) (pt 4.15 4.15) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.075 -4.15) (pt 4.15 -4.15) (width 0.2))
		)
	)
	(symbolDef "EEE-FK1J470P" (originalName "EEE-FK1J470P")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 100 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 0 mils -35 mils) (rotation 0]) (justify "UpperLeft") (textStyleRef "Default"))
		))
		(pin (pinNum 2) (pt 500 mils 0 mils) (rotation 180) (pinLength 100 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 500 mils -35 mils) (rotation 0]) (justify "UpperRight") (textStyleRef "Default"))
		))
		(line (pt 200 mils 100 mils) (pt 200 mils -100 mils) (width 6 mils))
		(line (pt 200 mils -100 mils) (pt 230 mils -100 mils) (width 6 mils))
		(line (pt 230 mils -100 mils) (pt 230 mils 100 mils) (width 6 mils))
		(line (pt 230 mils 100 mils) (pt 200 mils 100 mils) (width 6 mils))
		(line (pt 180 mils 50 mils) (pt 140 mils 50 mils) (width 6 mils))
		(line (pt 160 mils 70 mils) (pt 160 mils 30 mils) (width 6 mils))
		(line (pt 100 mils 0 mils) (pt 200 mils 0 mils) (width 6 mils))
		(line (pt 300 mils 0 mils) (pt 400 mils 0 mils) (width 6 mils))
		(poly (pt 300 mils 100 mils) (pt 300 mils -100 mils) (pt 270 mils -100 mils) (pt 270 mils 100 mils) (pt 300 mils 100 mils) (width 10  mils))
		(attr "RefDes" "RefDes" (pt 350 mils 250 mils) (justify 24) (isVisible True) (textStyleRef "Default"))

	)
	(compDef "EEE-FK1J470P" (originalName "EEE-FK1J470P") (compHeader (numPins 2) (numParts 1) (refDesPrefix C)
		)
		(compPin "1" (pinName "+") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(compPin "2" (pinName "-") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "EEE-FK1J470P"))
		(attachedPattern (patternNum 1) (patternName "CAPAE830X1050N")
			(numPads 2)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
			)
		)
		(attr "Manufacturer_Name" "Panasonic")
		(attr "Manufacturer_Part_Number" "EEE-FK1J470P")
		(attr "Mouser Part Number" "667-EEE-FK1J470P")
		(attr "Mouser Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=667-EEE-FK1J470P")
		(attr "RS Part Number" "")
		(attr "RS Price/Stock" "")
		(attr "Allied_Number" "70258125")
		(attr "Allied Price/Stock" "https://www.alliedelec.com/panasonic-eee-fk1j470p/70258125/")
		(attr "Description" "Aluminum Electrolytic Capacitors - SMD 47uF 63V")
		(attr "Datasheet Link" "https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/ABA0000C1181.pdf")
		(attr "Height" "10.5 mm")
	)

)
