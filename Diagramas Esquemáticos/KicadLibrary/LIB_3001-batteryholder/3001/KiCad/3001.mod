PCBNEW-LibModule-V1  2020-02-29 12:40:18
# encoding utf-8
Units mm
$INDEX
3001
$EndINDEX
$MODULE 3001
Po 0 0 0 15 5e5a5bb2 00000000 ~~
Li 3001
Cd 3001
Kw Undefined or Miscellaneous
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "U**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "3001"
DA 0 -0 7.43 0 -1800 0.2 21
DA 0 -0 -7.43 0 -1800 0.2 21
DA 0 -0 -7.43 0 1800 0.2 24
DA 0 -0 -7.43 0 -1800 0.2 24
$PAD
Po -6.605 0
Sh "1" C 2.8 2.8 0 0 900
Dr 1.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 6.605 0
Sh "2" C 2.8 2.8 0 0 900
Dr 1.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0 0
Sh "3" R 3.96 3.96 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE 3001
$EndLIBRARY
