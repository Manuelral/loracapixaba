!PADS-POWERPCB-V9.5-METRIC! DESIGN DATABASE ASCII FILE 1.0
*PARTDECAL*  ITEMS

3001 M 0 0 4 3 2 0 2
OPEN 2 0.2 0 26
7.43  0     0   1800 -7.43 -7.43 7.43  7.43 
-7.43 0    
OPEN 2 0.2 0 26
-7.43 0     1800 1800 -7.43 -7.43 7.43  7.43 
7.43  0    
OPEN 2 0.2 0 27
-7.43 0     1800 -1800 -7.43 -7.43 7.43  7.43 
7.43  0    
OPEN 2 0.2 0 27
-7.43 0     1800 1800 -7.43 -7.43 7.43  7.43 
7.43  0    
VALUE 0 0 0 1 1.27 0.127 N LEFT DOWN
Regular <Romansim Stroke Font>
Ref.Des.
VALUE 0 0 0 1 1.27 0.127 N LEFT UP
Regular <Romansim Stroke Font>
Part Type
T-6.605 0     -6.605 0     1
T6.605 0     6.605 0     2
T0     0     0     0     3
PAD 0 3
-2 2.8 R   1.85
-1 2.8 R  
0  2.8 R  
PAD 3 3
-2 3.96 S   0   0
-1 0   R  
0  0   R
*END*     OF ASCII OUTPUT FILE
