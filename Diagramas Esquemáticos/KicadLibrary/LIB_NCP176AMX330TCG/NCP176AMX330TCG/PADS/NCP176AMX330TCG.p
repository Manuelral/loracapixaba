*PADS-LIBRARY-PART-TYPES-V9*

NCP176AMX330TCG NCP176AMX330TCG I ANA 9 1 0 0 0
TIMESTAMP 2020.02.07.11.38.13
"Manufacturer_Name" ON Semiconductor
"Manufacturer_Part_Number" NCP176AMX330TCG
"Mouser Part Number" 863-NCP176AMX330TCG
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=863-NCP176AMX330TCG
"RS Part Number" 9209827P
"RS Price/Stock" http://uk.rs-online.com/web/p/products/9209827P
"Description" ON Semiconductor NCP176AMX330TCG, LDO Voltage Regulator, 500mA, 3.3 V +/-0.8%, 1.4  5.5 Vin, 6-Pin XDFN
"Datasheet Link" https://www.mouser.com/datasheet/2/308/NCP176-D-1551588.pdf
"Geometry.Height" 0.45mm
GATE 1 7 0
NCP176AMX330TCG
1 0 U OUT
2 0 U FB
3 0 U GND_1
4 0 U EN
5 0 U N/C
6 0 U IN
7 0 U GND_2

*END*
*REMARK* SamacSys ECAD Model
525886/180600/2.45/7/4/Integrated Circuit
