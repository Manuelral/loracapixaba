*PADS-LIBRARY-PART-TYPES-V9*

FDN86246 SOT95P237X122-3N I TRX 9 1 0 0 0
TIMESTAMP 2020.06.12.18.32.53
"Manufacturer_Name" ON Semiconductor
"Manufacturer_Part_Number" FDN86246
"Arrow Part Number" FDN86246
"Arrow Price/Stock" https://www.arrow.com/en/products/fdn86246/on-semiconductor
"Mouser Part Number" 512-FDN86246
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/ON-Semiconductor-Fairchild/FDN86246?qs=dIxESgyDOrhoaLoGKHFSzg%3D%3D
"Description" FDN86246 N-Channel MOSFET, 1.6 A, 150 V PowerTrench, 3-Pin SOT-23 ON Semiconductor
"Datasheet Link" https://www.onsemi.com/pub/Collateral/FDN86246-D.pdf
"Geometry.Height" 1.22mm
GATE 1 3 0
FDN86246
1 0 U G
3 0 U D
2 0 U S

*END*
*REMARK* SamacSys ECAD Model
1146455/180600/2.46/3/3/MOSFET (N-Channel)
