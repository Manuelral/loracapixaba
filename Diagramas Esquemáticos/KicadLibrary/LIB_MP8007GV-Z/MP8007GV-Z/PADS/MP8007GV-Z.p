*PADS-LIBRARY-PART-TYPES-V9*

MP8007GV-Z MP8007GVZ I ANA 9 1 0 0 0
TIMESTAMP 2020.02.04.17.44.01
"Manufacturer_Name" Monolithic Power Systems (MPS)
"Manufacturer_Part_Number" MP8007GV-Z
"Mouser Part Number" 946-MP8007GV-Z
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=946-MP8007GV-Z
"RS Part Number" 
"RS Price/Stock" 
"Description" Power Switch ICs - POE / LAN 802.3af-Compatible PoE PD Interface
"Datasheet Link" https://ms.componentsearchengine.com/Datasheets/1/MP8007GV-Z.pdf
"Geometry.Height" 1mm
GATE 1 29 0
MP8007GV-Z
1 0 U AUX
2 0 U DET
3 0 U N/C_1
4 0 U VDD
5 0 U FB2
6 0 U MODE
7 0 U FB1
8 0 U ILIM
9 0 U AGND
10 0 U VCC
11 0 U N/C_2
12 0 U SW_1
13 0 U SW_2
14 0 U N/C_3
15 0 U N/C_4
16 0 U GND_1
17 0 U GND_2
18 0 U PG
19 0 U N/C_5
20 0 U RTN_1
21 0 U RTN_2
22 0 U N/C_6
23 0 U VSS_1
24 0 U VSS_2
25 0 U FTY
26 0 U CLASS
27 0 U T2P
28 0 U N/C_7
29 0 U EP

*END*
*REMARK* SamacSys ECAD Model
726411/180600/2.45/29/4/Integrated Circuit
