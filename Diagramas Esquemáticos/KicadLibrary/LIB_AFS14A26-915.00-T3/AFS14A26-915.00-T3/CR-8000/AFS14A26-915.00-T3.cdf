(part "AFS14A26-915_00-T3"
    (packageRef "AFS14A2691500T3")
    (interface
        (port "A1" (symbPinId 1) (portName "OUTPUT") (portType INOUT))
        (port "B1" (symbPinId 2) (portName "GROUND_1") (portType INOUT))
        (port "C1" (symbPinId 3) (portName "INPUT") (portType INOUT))
        (port "D1" (symbPinId 4) (portName "GROUND_2") (portType INOUT))
        (port "E1" (symbPinId 5) (portName "GROUND_3") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "155")
    (property "Manufacturer_Name" "ABRACON")
    (property "Manufacturer_Part_Number" "AFS14A26-915.00-T3")
    (property "Mouser_Part_Number" "815-AFS14A26-915-T3")
    (property "Mouser_Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=815-AFS14A26-915-T3")
    (property "RS_Part_Number" "")
    (property "RS_Price/Stock" "")
    (property "Description" "ABRACON - AFS14A26-915.00-T3 - SAW FILTER, 915MHZ, SMD-5")
    (property "Datasheet_Link" "https://abracon.com/Filters/SAW%20FILTERS/AFS14A26-915.00-T3.pdf")
    (property "symbolName1" "AFS14A26-915_00-T3")
)
