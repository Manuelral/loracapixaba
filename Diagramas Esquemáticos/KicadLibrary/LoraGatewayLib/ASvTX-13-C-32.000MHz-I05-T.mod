PCBNEW-LibModule-V1  2020-02-12 11:48:42
# encoding utf-8
Units mm
$INDEX
ASvTX13C32000MHzI05T
$EndINDEX
$MODULE ASvTX13C32000MHzI05T
Po 0 0 0 15 5e43e61a 00000000 ~~
Li ASvTX13C32000MHzI05T
Cd ASvTX-13-C-32.000MHz-I05-T-1
Kw Crystal or Oscillator
Sc 0
At SMD
AR 
Op 0 0 0
T0 -0.200 0.025 1.27 1.27 0 0.254 N V 21 N "Y**"
T1 -0.200 0.025 1.27 1.27 0 0.254 N I 21 N "ASvTX13C32000MHzI05T"
DS -1 -0.8 1 -0.8 0.2 24
DS 1 -0.8 1 0.8 0.2 24
DS 1 0.8 -1 0.8 0.2 24
DS -1 0.8 -1 -0.8 0.2 24
DS -2.6 -2 2.2 -2 0.1 24
DS 2.2 -2 2.2 2.05 0.1 24
DS 2.2 2.05 -2.6 2.05 0.1 24
DS -2.6 2.05 -2.6 -2 0.1 24
DS -1.5 1 -1.5 1 0.2 21
DS -1.6 1 -1.6 1 0.2 21
DA -1.55 1 -1.500 1 -1800 0.2 21
DA -1.55 1 -1.600 1 -1800 0.2 21
$PAD
Po -0.900 0.6
Sh "1" R 0.600 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.900 0.6
Sh "2" R 0.600 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.900 -0.6
Sh "3" R 0.600 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.900 -0.6
Sh "4" R 0.600 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE ASvTX13C32000MHzI05T
$EndLIBRARY
