PCBNEW-LibModule-V1  2020-02-10 18:43:09
# encoding utf-8
Units mm
$INDEX
UFLRSMT160
$EndINDEX
$MODULE UFLRSMT160
Po 0 0 0 15 5e41a43d 00000000 ~~
Li UFLRSMT160
Cd U.FL-R-SMT-1(60)-2
Kw Inductor
Sc 0
At SMD
AR 
Op 0 0 0
T0 0.000 0.375 1.27 1.27 0 0.254 N V 21 N "L**"
T1 0.000 0.375 1.27 1.27 0 0.254 N I 21 N "UFLRSMT160"
DS -1.3 -1.3 1.3 -1.3 0.2 24
DS 1.3 -1.3 1.3 1.3 0.2 24
DS 1.3 1.3 -1.3 1.3 0.2 24
DS -1.3 1.3 -1.3 -1.3 0.2 24
DS -3 -2.3 3 -2.3 0.1 24
DS 3 -2.3 3 3.05 0.1 24
DS 3 3.05 -3 3.05 0.1 24
DS -3 3.05 -3 -2.3 0.1 24
DS -1.3 -1.3 1.3 -1.3 0.1 21
$PAD
Po -1.475 -0
Sh "1" R 1.050 2.200 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.475 -0
Sh "2" R 1.050 2.200 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 1.525
Sh "3" R 1.000 1.050 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE UFLRSMT160
$EndLIBRARY
