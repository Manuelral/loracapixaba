PCBNEW-LibModule-V1  2020-02-16 11:50:27
# encoding utf-8
Units mm
$INDEX
SON65P200X200X55-6N
$EndINDEX
$MODULE SON65P200X200X55-6N
Po 0 0 0 15 5e492c83 00000000 ~~
Li SON65P200X200X55-6N
Cd UDFN-6L
Kw Undefined or Miscellaneous
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "U**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "SON65P200X200X55-6N"
DS -1.625 -1.275 1.625 -1.275 0.05 24
DS 1.625 -1.275 1.625 1.275 0.05 24
DS 1.625 1.275 -1.625 1.275 0.05 24
DS -1.625 1.275 -1.625 -1.275 0.05 24
DS -1 -1 1 -1 0.1 24
DS 1 -1 1 1 0.1 24
DS 1 1 -1 1 0.1 24
DS -1 1 -1 -1 0.1 24
DS -1 -0.5 -0.5 -1 0.1 24
DC -1.375 -1.3 -1.25 -1.3 0.254 21
$PAD
Po -0.85 -0.65
Sh "1" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.85 0
Sh "2" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.85 0.65
Sh "3" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.85 0.65
Sh "4" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.85 0
Sh "5" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.85 -0.65
Sh "6" R 0.3 1.05 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE SON65P200X200X55-6N
$EndLIBRARY
