*PADS-LIBRARY-PCB-DECALS-V9*

HDRV2W64P0X254_1X2_508X248X864P M 0 0 0 2 3 0 2 2 0
TIMESTAMP 2020.06.13.00.06.41
0 0 0 0 1.27 0.127 1 0 34 "Regular <Romansim Stroke Font>"
REF-DES
0 0 0 0 1.27 0.127 1 32 35 "Regular <Romansim Stroke Font>"
PART-TYPE
CLOSED 5 0.05 20 -1
1.52 -1.49
-4.06 -1.49
-4.06 1.49
1.52 1.49
1.52 -1.49
CLOSED 5 0.1 27 -1
1.27 -1.24
-3.81 -1.24
-3.81 1.24
1.27 1.24
1.27 -1.24
OPEN 5 0.2 26 -1
0 -1.24
-3.81 -1.24
-3.81 1.24
1.27 1.24
1.27 0
T0 0 0 0 1
T-2.54 0 -2.54 0 2
PAD 0 3 P 1.1
-2 1.65 R
-1 1.65 R
0 1.65 R
PAD 1 3 P 1.1
-2 1.65 S 0
-1 1.65 R
0 1.65 S 0

*END*
