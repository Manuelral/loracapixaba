SamacSys ECAD Model
925269/180600/2.46/2/3/Connector

DESIGNSPARK_INTERMEDIATE_ASCII

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "c165_h110"
		(holeDiam 1.1)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.65) (shapeHeight 1.65))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.65) (shapeHeight 1.65))
	)
	(padStyleDef "s165_h110"
		(holeDiam 1.1)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.65) (shapeHeight 1.65))
		(padShape (layerNumRef 16) (padShapeType Rect)  (shapeWidth 1.65) (shapeHeight 1.65))
	)
	(textStyleDef "Default"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 50 mils)
			(strokeWidth 5 mils)
		)
	)
	(patternDef "HDRV2W64P0X254_1X2_508X248X864" (originalName "HDRV2W64P0X254_1X2_508X248X864")
		(multiLayer
			(pad (padNum 1) (padStyleRef s165_h110) (pt 0, 0) (rotation 90))
			(pad (padNum 2) (padStyleRef c165_h110) (pt -2.54, 0) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0, 0) (textStyleRef "Default") (isVisible True))
		)
		(layerContents (layerNumRef 30)
			(line (pt 1.52 -1.49) (pt -4.06 -1.49) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt -4.06 -1.49) (pt -4.06 1.49) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt -4.06 1.49) (pt 1.52 1.49) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt 1.52 1.49) (pt 1.52 -1.49) (width 0.05))
		)
		(layerContents (layerNumRef 28)
			(line (pt 1.27 -1.24) (pt -3.81 -1.24) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -3.81 -1.24) (pt -3.81 1.24) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -3.81 1.24) (pt 1.27 1.24) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt 1.27 1.24) (pt 1.27 -1.24) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 0 -1.24) (pt -3.81 -1.24) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.81 -1.24) (pt -3.81 1.24) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.81 1.24) (pt 1.27 1.24) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 1.27 1.24) (pt 1.27 0) (width 0.2))
		)
	)
	(symbolDef "HMTSW-102-07-G-S-230" (originalName "HMTSW-102-07-G-S-230")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Default"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Default"))
		))
		(line (pt 200 mils 100 mils) (pt 600 mils 100 mils) (width 6 mils))
		(line (pt 600 mils 100 mils) (pt 600 mils -200 mils) (width 6 mils))
		(line (pt 600 mils -200 mils) (pt 200 mils -200 mils) (width 6 mils))
		(line (pt 200 mils -200 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Default"))

	)
	(compDef "HMTSW-102-07-G-S-230" (originalName "HMTSW-102-07-G-S-230") (compHeader (numPins 2) (numParts 1) (refDesPrefix J)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "HMTSW-102-07-G-S-230"))
		(attachedPattern (patternNum 1) (patternName "HDRV2W64P0X254_1X2_508X248X864")
			(numPads 2)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
			)
		)
		(attr "Manufacturer_Name" "SAMTEC")
		(attr "Manufacturer_Part_Number" "HMTSW-102-07-G-S-230")
		(attr "Arrow Part Number" "HMTSW-102-07-G-S-230")
		(attr "Arrow Price/Stock" "")
		(attr "Mouser Part Number" "200-HMTSW10207GS230")
		(attr "Mouser Price/Stock" "https://www.mouser.co.uk/ProductDetail/Samtec/HMTSW-102-07-G-S-230?qs=Cqqh%252BS766wnc6NC0aw7pGg%3D%3D")
		(attr "Description" "Board to Board & Mezzanine Connectors .100" High-Temp Variable Post Height Terminal Strip")
		(attr "Datasheet Link" "http://suddendocs.samtec.com/catalog_english/mtsw.pdf")
		(attr "Height" "8.64 mm")
	)

)
